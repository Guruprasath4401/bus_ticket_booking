const jwt = require('jsonwebtoken');

const auth = function (req, res, next){
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDcxYjYxNmUwYThmOTBlN2MwM2M1MmQiLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2MTgwNjQ5MjB9.OYRx1PmB1ckDpMo89iqIMlfbey5L2CxygdO6jn4XEXA'
    if(!token){
        return res.status(401).send('Access Denied for this admin');
    }

    try{
      const decoded = jwt.verify(token, "jwtPrivateKey");
      req.user = decoded;
      next();
    }
    catch(err){
       return res.status(403).send("Access Denied!");
    }
}

const adminCheck = function (req, res, next) {
    if (!req.user.isAdmin) 
        return res.status(403).send('Access Denied!');
    next();
}

module.exports = {
    auth: auth,
    adminCheck: adminCheck
}
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import Homepage from './components/Homepage/Homepage'
import RouteSelection from './components/RouteSelection/RouteSelection'

import LogiSignup from './components/Login-Signup/LogOrsign'
import AdminPage from './components/AdminPage/AdminPage'
import Passenger from './components/Passenger/Passenger'
import Reset from './components/Reset/ResetData'
import './App.css';
import ViewOpened from './components/ViewOpened/ViewOpened';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact render={props => <Homepage {...props} />} />
         
          <Route path="/Admin" exact render={props => <LogiSignup {...props} />} />
          <Route path="/Admin/home" exact render={props => <AdminPage {...props} />} />
          <Route path="/book" exact render={props => <RouteSelection {...props} />} />
          <Route path="/Admin/pass/:id" exact render={props => <Passenger {...props}/>}/>
          <Route path="/Admin/reset" exact render={props => <Reset {...props}/>}/>
          <Route path="/Admin/viewOpen" exact render={props => <ViewOpened {...props}/>}/>
          
          
        </Switch>
      </Router>
    </div>

  );
}

export default App;

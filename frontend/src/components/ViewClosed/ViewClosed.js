import React, { useState } from 'react'
import axios from 'axios'
import './ViewClosed.css'
import * as ReactBootStrap from 'react-bootstrap';
export default function ViewClosed() {
    const [tickets ,setTickets]=useState([])
    const [details, setDetail]= useState([])
    const handleSubmitDetails = () => {
        axios.get('http://localhost:8080/bus/ticket/viewClosed').then((res)=>{
         
            setTickets(res.data);
        },[]);
    }
        const renderTicket = (ticket,index) => {   
            var id= ticket._id;
                axios.get(`http://localhost:8080/bus/pass/${id.toString()}`).then((res)=>{
                    setDetail({...res.data});
                    console.log(details)
        
                },[])    
            return(
                <tr className="btn btn-info " key={index} > 
                    <td style={{backgroundColor:"darkblue", borderColor:"darkblue",width:"40px"}}>{ticket.seatID}</td>
                    {/* {
                    axios.get(`http://localhost:8080/bus/pass/${ticket.passengerObj}`).then((resp)=>{
                        setDetail(resp.data);
                    })
                } */}
                </tr>
            )           
        }
    return (
        <div className="ss">
            <div className="row">
                <div className="columna">
                    <div className="plane">
                        {/* <form >
                           
                        </form> */}
                    </div>
                </div>
                <div className="column2">
                    <div className="seatInfo">
                        
                        <div>
                            <a href="/Admin/pass">
                            <button onClick={ handleSubmitDetails()}  style={{marginLeft: "0px",width:"100%",height:"60px"}} className="btn btn-info seatBT">
                              View Closed Tickets
                            </button>
                            </a>
                       <ReactBootStrap.Table  >
                              <br></br> 
                            <tbody class="info"> <span class="extra-info">                         
                        <ul >
                            <li>Name:{details.name}</li>
                            <li>Age : { details.age}</li>
                            <li>Email ID:{ details.email}</li>
                        </ul>   
                            </span>
                            {tickets.map(renderTicket)}
                            </tbody>
                             </ReactBootStrap.Table>
         
                         </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
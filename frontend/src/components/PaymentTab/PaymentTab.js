import React from "react";
import './PaymentTab.css'
import "react-credit-cards/es/styles-compiled.css";
export default class App extends React.Component {
    handleCallback = ({ issuer }, isValid) => {
        if (isValid) {
            this.setState({ issuer });
        }
    };

    handleInputFocus = ({ target }) => {
        this.setState({
            focused: target.name
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        const formData = [...e.target.elements]
            .filter(d => d.name)
            .reduce((acc, d) => {
                acc[d.name] = d.value;
                return acc;    
            }, {});

        this.setState({ formData });
        this.form.reset();
        window.location.reload();
        

    };
    renderNamesOfPassenger = () => {
        let passArray = localStorage.getItem('nameData')
        if (passArray) {
            let nameArray = JSON.parse(passArray)
            return nameArray.map((name, idx) => {
                return (
                    <p key={idx}>{name}</p>
                )
            })
        }
    }
    renderNamesOfEmail= () => {
        let passArray = localStorage.getItem('emailData')
        if (passArray) {
            let nameArray = JSON.parse(passArray)
            return nameArray.map((name, idx) => {
                return (
                    <p key={idx}>{name}</p>
                )
            })
        }
    }
    renderNamesOfPhone = () => {
        let passArray = localStorage.getItem('phoneData')
        if (passArray) {
            let nameArray = JSON.parse(passArray)
            return nameArray.map((name, idx) => {
                return (
                    <p key={idx}>{name}</p>
                )
            })
        }
    }
    renderNamesOfAge = () => {
        let passArray = localStorage.getItem('ageData')
        if (passArray) {
            let nameArray = JSON.parse(passArray)
            return nameArray.map((age, idx) => {
                return (
                    <p key={idx}>{age}</p>
                )
            })
        }
    }
    renderNamesOfGender = () => {
        let passArray = localStorage.getItem('genderData')
        if (passArray) {
            let nameArray = JSON.parse(passArray)
            return nameArray.map((name, idx) => {
                return (
                    <p key={idx}>{name}</p>
                )
            })
        }
    }

    renderSeatNumbers = () => {
        let seatArray = localStorage.getItem('reservedSeats')
        if (seatArray) {
            let seaArr = JSON.parse(seatArray)
            return seaArr.map((seat, idx) => {
                return (
                    <p key={idx}>{seat}</p>
                )
            })
        }
    }
    
    moveToTicketPage = (e) => {
        e.preventDefault()
        window.localStorage.clear();
        console.log(e);
        alert("Ticket has been Booked succesfully");
        window.location.href = "/"
    }

    render() {
    
        return (
            <div className="paym">
                <div className="row">
                    <div className="columnTwo">
                        <h3>BUS BOOK</h3>
                        <div>
                            <p>BOOKING DETAILS</p>
                            <div className="row">
                                <div className="col-6 pt">
                                    <p className="hdng">Username</p>                               
                                    <hr className="hr3" />  

                                    <p className="hdng">Passenger Name</p>
                                    {this.renderNamesOfPassenger()}
                                    <hr className="hr3" />

                                    <p className="hdng">Passenger Email</p>
                                    {this.renderNamesOfEmail()}
                                    <hr className="hr3" />

                                    <p className="hdng">Passenger Age</p>
                                    {this.renderNamesOfAge()}
                                </div>
                                
                                <div className="col-6">
                                   
                                    <p className="usrName">
                                    {this.renderNamesOfPassenger()}</p>
                                    <hr className="hr3" />

                                    <p className="hdng">Seat No</p>
                                    {this.renderSeatNumbers()}
                                    <hr className="hr3" />

                                    <p className="hdng">Phone no</p>
                                    {this.renderNamesOfPhone()}
                                    <hr className="hr3" />

                                    <p className="hdng">Gender</p>
                                    {this.renderNamesOfGender()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <form>
                    <div className="">
                            <button  style={{marginBottom: "5px",marginLeft:"100px"}} onClick={e => this.moveToTicketPage(e)} className="btn btn-light btCustom">Submit</button>
                    </div>
                    </form>
                            
                    </div>
            </div>
        );
    }
}

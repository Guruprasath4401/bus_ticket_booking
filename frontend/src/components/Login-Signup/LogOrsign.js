import React, { useState } from 'react'
import * as logFunc from './loginFunctions.js'
import './logOrsign.css'
//import { FaFacebookF, FaTwitterSquare } from "react-icons/fa";
export default function LogOrsign({ history }) {

    let [userData, setUserData] = useState({})
    console.log(userData);
    if(userData == null){
        window.location.href='./Admin'
    }
   
    const handleChangeEvent = (e, title) => {
        let value = e.target.value
        setUserData({ ...userData, [title]: value })

    }

    const submitData = e => {
        e.preventDefault()
        // console.log(userData)
        logFunc.logUserIn(userData)
            .then(response => response.data)
            .then(data => {
               // let { token } = data
               // alert("  "+ token)
               // sessionStorage.setItem('authToken', token)
                history.push('/Admin')
                window.location.href='/Admin/home'
                
                console.log(data);
            })
    }



    return (
        <div className="container">
            <section className="myform-area">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-60">
                            <div className="form-area login-form">
                               
                                    
                                 
                                    {/* <ul>
                                        <li><a href="/#" className="facebook"><FaFacebookF /></a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="/#" className="twitter"><FaTwitterSquare /></a></li>
                                    </ul> */}
                               
                                <div className="form-input">
                                    <h2>LOGIN</h2>
                                    <form onSubmit={(e) => { submitData(e) }}>
                                        <div class="form-group">
                                            <input className="loginInfo" type="email" name="name" required onChange={e => handleChangeEvent(e, 'email')} />
                                            <label>Email-Id</label>
                                        </div>
                                        <div class="form-group">
                                            <input className="loginInfo" type="password" name="password" required onChange={e => handleChangeEvent(e, 'password')} />
                                            <label>password</label>
                                        </div>
                                        <div class="myform-button">
                                            <button type="submit"  className="myform-btn">Login</button>
                                        </div>
                                       
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div >
    )
}

import React from 'react'
import './homepage.css'
export default function Homepage({ history }) {
    const enterSite = e => {
        e.preventDefault()
        history.push('/book')
    }
    return (
        <div className='container maint-cnt'>
            <div className="header-nav">
                <span className="mytext1">TICKET BOOK </span>
            </div>
        <div className="container">
            <div className="slogan">                  
        </div>
        <a href="/book" onClick={e => enterSite(e) } onclick={ window.localStorage.clear()} className="mainBtn">
                    <svg width="277" height="62">
                        <defs>
                            <linearGradient id="grad1">
                                <stop offset="0%" stopColor="#FF8282" />
                                <stop offset="100%" stopColor="#E178ED" />
                            </linearGradient>
                        </defs>
                        <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
                    </svg>
                <span >BOOK A TICKET !</span>
        </a>
            </div>
        </div>
    )
}


